const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

const apiRoutes = require('./routes/apiRoutes');

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(morgan('tiny'));

app.use('/api', apiRoutes);

app.listen(8080, (err, res) => {
    console.log('The server works at 8080')
});