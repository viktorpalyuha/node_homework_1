const fs = require('fs');
const path = require('path');
const dirPath = path.join(__dirname, '..', 'files');

module.exports.createFile = (req, res) => {
  let { filename, content } = req.body;

  fs.writeFile(path.join(dirPath, filename), content, (err) => {
    if (err) {
      res.status(500).json({ message: 'Server error' });
    } else {
      res.status(200).json({
        message: 'File created successfully'
      });
    }
  });
};

module.exports.getFile = (req, res) => {
  let filename = req.params.filename;
  const extension = path.extname(filename);

  fs.readFile(path.join(dirPath, filename), 'utf8', (err, content) => {
    if (err) {
      if (err.code === 'ENOENT') {
        return res.status(400).json({ message: `No file with '${filename}' filename found` });
      }
      res.status(500).json({
        message: 'Server error'
      });
    } else {
      fs.stat(path.join(dirPath, filename), (err, stats) => {
        if (err) {
          res.status(500).json({
            message: 'Server error'
          });
        } else {
          res.status(200).json({
            message: 'Success',
            filename,
            content,
            extension: extension.split('.')[1],
            uploadedDate: stats.birthtime
          });
        }
      });
    }
  });
};

module.exports.getFiles = (_, res) => {
  fs.readdir(dirPath, (err, files) => {
    if (err) {
      res.status(500).json({
        message: 'Server error'
      });
    } else {
      res.status(200).json({
        message: 'Success',
        files
      });
    }
  });
};
