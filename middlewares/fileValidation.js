const path = require('path');
const fs = require('fs');
const dirPath = path.join(__dirname, '..', 'files');

module.exports.validateFile = (req, res, next) => {
  const { filename, content } = req.body;
  const allowedExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

  if (!filename) {
    return res.status(400).json({
      message: "Please specify 'filename' parameter"
    });
  }

  if (!content) {
    return res.status(400).json({
      message: "Please, specify 'content' parameter"
    });
  }

  const filenameExtension = path.extname(filename);

  if (!allowedExtensions.includes(filenameExtension)) {
    return res.status(400).json({
      message: 'Please, specify another extension'
    });
  }

  if (!fs.existsSync(dirPath)) {
    fs.mkdir(dirPath, (err) => {
      if (err) {
        return res.status(500).json({
          message: 'Server error'
        });
      }
    });
  }

  next();
};
