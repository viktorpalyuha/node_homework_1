const express = require('express');
const router = express.Router();
const apiController = require('../controllers/apiController');
const fileValidation = require('../middlewares/fileValidation');

router.post('/files', fileValidation.validateFile, apiController.createFile);

router.get('/files/:filename', apiController.getFile);

router.get('/files', apiController.getFiles);

module.exports = router;
